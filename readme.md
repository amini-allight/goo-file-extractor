# GOO File Extractor

A simple tool to extract and print out the contents of ELEGOO .goo SLA print files. Used for investigating the root cause of print issues.

## Building

```sh
mkdir -p build
cd build
cmake ..
make -j$(nproc)
```

## Usage

```sh
cd bin
./goo-extractor my-file.goo
```

## Credit & License

Created by Amini Allight based on structs originally created by [Felix Reißmann](https://github.com/Felix-Rm). Licensed under the AGPL 3.0. This project is not endorsed by or affiliated with ELEGOO.
