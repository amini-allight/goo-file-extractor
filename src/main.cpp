#include <iostream>
#include <fstream>
#include <streambuf>
#include <cstdint>
#include <string>

using namespace std;

template<typename T>
T invertByteOrder(const T& input)
{
    auto inBytes = reinterpret_cast<const uint8_t*>(&input);

    T output;

    auto outBytes = reinterpret_cast<uint8_t*>(&output);

    for (size_t i = 0; i < sizeof(T); i++)
    {
        outBytes[sizeof(T) - (i + 1)] = inBytes[i];
    }

    return output;
}

#define INVERT(_fieldName) \
_fieldName = invertByteOrder(_fieldName)
#define DISPLAY(_fieldName) \
cout << #_fieldName << ": " << _fieldName << endl;
#define DISPLAY_BYTE(_fieldName) \
cout << #_fieldName << ": " << static_cast<int>(_fieldName) << endl;
#define DISPLAY_CHAR_ARRAY(_fieldName) \
cout << #_fieldName << ": " << string(_fieldName, sizeof(_fieldName)) << endl;
#define DISPLAY_BYTE_ARRAY(_fieldName) \
cout << #_fieldName << ": "; \
for (size_t i = 0; i < sizeof(_fieldName); i++) \
{ \
    cout << static_cast<int>(_fieldName[i]) << " "; \
} \
cout << endl;

static string getFile(const string& path)
{
    ifstream file(path);

    return string(istreambuf_iterator<char>(file), istreambuf_iterator<char>());
}

#pragma pack(1)
struct header_info
{
    char version[4];
    uint8_t magic_tag[8];
    char software_info[32];
    char software_version[24];
    char file_time[24];
    char printer_name[32];
    char printer_type[32];
    char resin_profile_name[32];
    uint16_t aa_level;
    uint16_t grey_level;
    uint16_t blur_level;
    uint16_t small_preview[116 * 116];
    uint8_t delimiter_1[2];
    uint16_t big_preview[290 * 290];
    uint8_t delimiter_2[2];
    uint32_t total_layers;
    uint16_t x_resolution;
    uint16_t y_resolution;
    uint8_t x_mirror;
    uint8_t y_mirror;
    float x_platform_size_mm;
    float y_platform_size_mm;
    float z_platform_size_mm;
    float layer_thickness_mm;
    float common_exposure_time_s;
    bool exposure_delivery_time_static;
    float turn_off_time_s;
    float bottom_before_lift_time_s;
    float bottom_after_lift_time_s;
    float bottom_after_retract_time_s;
    float before_lift_time_s;
    float after_lift_time_s;
    float after_retract_time_s;
    float bottom_exposure_time_s;
    uint32_t bottom_layers;
    float bottom_lift_distance_mm;
    float bottom_lift_speed_mm_min;
    float lift_distance_mm;
    float lift_speed_mm_min;
    float bottom_retract_distance_mm;
    float bottom_retract_speed_mm_min;
    float retract_distance_mm;
    float retract_speed_mm_min;
    float bottom_second_lift_distance_mm;
    float bottom_second_lift_speed_mm_min;
    float second_lift_distance_mm;
    float second_lift_speed_mm_min;
    float bottom_second_retract_distance_mm;
    float bottom_second_retract_speed_mm_min;
    float second_retract_distance_mm;
    float second_retract_speed_mm_min;
    uint16_t bottom_light_pwm;
    uint16_t light_pwm;
    bool advance_mode_layer_definition;
    uint32_t printing_time_s;
    float total_volume_mm3;
    float total_weight_g;
    float total_price;
    uint8_t price_unit[8];
    uint32_t layer_content_offset;
    bool grayscale_level;
    uint16_t transition_layers;

    void invertFieldByteOrders()
    {
        INVERT(aa_level);
        INVERT(grey_level);
        INVERT(blur_level);
        for (size_t i = 0; i < sizeof(small_preview) / sizeof(uint16_t); i++)
        {
            INVERT(small_preview[i]);
        }
        for (size_t i = 0; i < sizeof(big_preview) / sizeof(uint16_t); i++)
        {
            INVERT(big_preview[i]);
        }
        INVERT(total_layers);
        INVERT(x_resolution);
        INVERT(y_resolution);
        INVERT(x_platform_size_mm);
        INVERT(y_platform_size_mm);
        INVERT(z_platform_size_mm);
        INVERT(layer_thickness_mm);
        INVERT(common_exposure_time_s);
        INVERT(turn_off_time_s);
        INVERT(bottom_before_lift_time_s);
        INVERT(bottom_after_lift_time_s);
        INVERT(bottom_after_retract_time_s);
        INVERT(before_lift_time_s);
        INVERT(after_lift_time_s);
        INVERT(after_retract_time_s);
        INVERT(bottom_exposure_time_s);
        INVERT(bottom_layers);
        INVERT(bottom_lift_distance_mm);
        INVERT(bottom_lift_speed_mm_min);
        INVERT(lift_distance_mm);
        INVERT(lift_speed_mm_min);
        INVERT(bottom_retract_distance_mm);
        INVERT(bottom_retract_speed_mm_min);
        INVERT(retract_distance_mm);
        INVERT(retract_speed_mm_min);
        INVERT(bottom_second_lift_distance_mm);
        INVERT(bottom_second_lift_speed_mm_min);
        INVERT(second_lift_distance_mm);
        INVERT(second_lift_speed_mm_min);
        INVERT(bottom_second_retract_distance_mm);
        INVERT(bottom_second_retract_speed_mm_min);
        INVERT(second_retract_distance_mm);
        INVERT(second_retract_speed_mm_min);
        INVERT(bottom_light_pwm);
        INVERT(light_pwm);
        INVERT(printing_time_s);
        INVERT(total_volume_mm3);
        INVERT(total_weight_g);
        INVERT(total_price);
        INVERT(layer_content_offset);
        INVERT(transition_layers);
    }
};
static_assert(sizeof(header_info) == 195477, "struct is not packed");

static ostream& operator<<(ostream& out, const header_info& header)
{
    DISPLAY_CHAR_ARRAY(header.version);
    DISPLAY_BYTE_ARRAY(header.magic_tag);
    DISPLAY_CHAR_ARRAY(header.software_info);
    DISPLAY_CHAR_ARRAY(header.software_version);
    DISPLAY_CHAR_ARRAY(header.file_time);
    DISPLAY_CHAR_ARRAY(header.printer_name);
    DISPLAY_CHAR_ARRAY(header.printer_type);
    DISPLAY_CHAR_ARRAY(header.resin_profile_name);
    DISPLAY(header.aa_level);
    DISPLAY(header.grey_level);
    DISPLAY(header.blur_level);
    // Omitted small_preview here
    DISPLAY_BYTE_ARRAY(header.delimiter_1);
    // Omitted big_preview here
    DISPLAY_BYTE_ARRAY(header.delimiter_2);
    DISPLAY(header.total_layers);
    DISPLAY(header.x_resolution);
    DISPLAY(header.y_resolution);
    DISPLAY_BYTE(header.x_mirror);
    DISPLAY_BYTE(header.y_mirror);
    DISPLAY(header.x_platform_size_mm);
    DISPLAY(header.y_platform_size_mm);
    DISPLAY(header.z_platform_size_mm);
    DISPLAY(header.layer_thickness_mm);
    DISPLAY(header.common_exposure_time_s);
    DISPLAY(header.exposure_delivery_time_static);
    DISPLAY(header.turn_off_time_s);
    DISPLAY(header.bottom_before_lift_time_s);
    DISPLAY(header.bottom_after_lift_time_s);
    DISPLAY(header.bottom_after_retract_time_s);
    DISPLAY(header.before_lift_time_s);
    DISPLAY(header.after_lift_time_s);
    DISPLAY(header.after_retract_time_s);
    DISPLAY(header.bottom_exposure_time_s);
    DISPLAY(header.bottom_layers);
    DISPLAY(header.bottom_lift_distance_mm);
    DISPLAY(header.bottom_lift_speed_mm_min);
    DISPLAY(header.lift_distance_mm);
    DISPLAY(header.lift_speed_mm_min);
    DISPLAY(header.bottom_retract_distance_mm);
    DISPLAY(header.bottom_retract_speed_mm_min);
    DISPLAY(header.retract_distance_mm);
    DISPLAY(header.retract_speed_mm_min);
    DISPLAY(header.bottom_second_lift_distance_mm);
    DISPLAY(header.bottom_second_lift_speed_mm_min);
    DISPLAY(header.second_lift_distance_mm);
    DISPLAY(header.second_lift_speed_mm_min);
    DISPLAY(header.bottom_second_retract_distance_mm);
    DISPLAY(header.bottom_second_retract_speed_mm_min);
    DISPLAY(header.second_retract_distance_mm);
    DISPLAY(header.second_retract_speed_mm_min);
    DISPLAY(header.bottom_light_pwm);
    DISPLAY(header.light_pwm);
    DISPLAY(header.advance_mode_layer_definition);
    DISPLAY(header.printing_time_s);
    DISPLAY(header.total_volume_mm3);
    DISPLAY(header.total_weight_g);
    DISPLAY(header.total_price);
    DISPLAY_BYTE_ARRAY(header.price_unit);
    DISPLAY(header.layer_content_offset);
    DISPLAY(header.grayscale_level);
    DISPLAY(header.transition_layers);

    return out;
}

struct layer_definition
{
    uint8_t reserved;
    bool pause_at_layer;
    float pause_lift_distance_mm;
    float position_mm;
    float exposure_time_s;
    float off_time_s;
    float before_lift_time_s;
    float after_lift_time_s;
    float after_retract_time_s;
    float lift_distance_mm;
    float lift_speed_mm_min;
    float second_lift_distance_mm;
    float second_lift_speed_mm_min;
    float retract_distance_mm;
    float retract_speed_mm_min;
    float second_retract_distance_mm;
    float second_retract_speed_mm_min;
    uint16_t light_pwm;
    uint8_t delimiter[2];

    void invertFieldByteOrders()
    {
        INVERT(pause_lift_distance_mm);
        INVERT(position_mm);
        INVERT(exposure_time_s);
        INVERT(off_time_s);
        INVERT(before_lift_time_s);
        INVERT(after_lift_time_s);
        INVERT(after_retract_time_s);
        INVERT(lift_distance_mm);
        INVERT(lift_speed_mm_min);
        INVERT(second_lift_distance_mm);
        INVERT(second_lift_speed_mm_min);
        INVERT(retract_distance_mm);
        INVERT(retract_speed_mm_min);
        INVERT(second_retract_distance_mm);
        INVERT(second_retract_speed_mm_min);
        INVERT(light_pwm);
    }
};
static_assert(sizeof(layer_definition) == 66, "struct is not packed");

static ostream& operator<<(ostream& out, const layer_definition& layer)
{
    DISPLAY_BYTE(layer.reserved);
    DISPLAY(layer.pause_at_layer);
    DISPLAY(layer.pause_lift_distance_mm);
    DISPLAY(layer.position_mm);
    DISPLAY(layer.exposure_time_s);
    DISPLAY(layer.off_time_s);
    DISPLAY(layer.before_lift_time_s);
    DISPLAY(layer.after_lift_time_s);
    DISPLAY(layer.after_retract_time_s);
    DISPLAY(layer.lift_distance_mm);
    DISPLAY(layer.lift_speed_mm_min);
    DISPLAY(layer.second_lift_distance_mm);
    DISPLAY(layer.second_lift_speed_mm_min);
    DISPLAY(layer.retract_distance_mm);
    DISPLAY(layer.retract_speed_mm_min);
    DISPLAY(layer.second_retract_distance_mm);
    DISPLAY(layer.second_retract_speed_mm_min);
    DISPLAY(layer.light_pwm);
    DISPLAY_BYTE_ARRAY(layer.delimiter);

    return out;
}

static constexpr uint8_t end_string[11] = {
    0x00,
    0x00,
    0x00,
    0x07,
    0x00,
    0x00,
    0x00,
    0x44,
    0x4c,
    0x50,
    0x00
};
#pragma pack()

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        cerr << "Usage: " << argv[0] << " file.goo" << endl;
        return 1;
    }

    string file = getFile(argv[1]);

    const char* readHead = file.data();

    auto header = *reinterpret_cast<const header_info*>(readHead);
    header.invertFieldByteOrders();
    readHead += sizeof(header_info);

    cout << header << endl << endl;

    for (size_t i = 0; i < header.total_layers; i++)
    {
        auto layer = *reinterpret_cast<const layer_definition*>(readHead);
        layer.invertFieldByteOrders();
        readHead += sizeof(layer_definition);

        cout << layer << endl << endl;

        auto dataSize = *reinterpret_cast<const uint32_t*>(readHead);
        INVERT(dataSize);
        // Correction for fields not included in the reported size, derived from the code at src/libslic3r/SLA/RasterBase.cpp:129 onwards
        dataSize += 6;

        readHead += dataSize;

        // Omitted actual raster data
    }

    return 0;
}
